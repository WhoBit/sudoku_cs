﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sudoku_3
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int PuzzleSize { get; }
        private int puzzlesize = 9;
        List<Cell> MyPuzzle = new List<Cell>();

        // Todo - Create a dictionary to hold all of the buttons created below - 2018-05-13

        public MainWindow()
        {
            InitializeComponent();

            Button btnClose = new Button();
            btnClose.Content = "Close";
            btnClose.Click += new RoutedEventHandler(btnClose_Click);
            grdPuzzle.Children.Add(btnClose);
            Grid.SetColumn(btnClose, 11);
            Grid.SetRow(btnClose, 0);

            int grdRow = 0;         // Row of the grid
            int grdColumn = 0;      // Column of the grid

            string row = "";        // Name of the puzzle row
            string column = "";     // Name of the puzzle column

            for (int i = 0; i < puzzlesize; i++)    // Row
            {
                row = (i + 1).ToString();

                switch (i)
                {
                    case 0:
                    case 1:
                    case 2:
                        grdRow = i + 1;                                                
                        break;
                    case 3:
                    case 4:
                    case 5:
                        grdRow = i + 2;         // Adjust for the spacing               
                        break;
                    case 6:
                    case 7:
                    case 8:
                        grdRow = i + 3;         // Adjust for the spacing
                        break;

                    default:
                        break;
                }

                
                for (int j = 0; j < puzzlesize; j++) // Column
                {
                    column = (j + 1).ToString();
                    switch (j)
                    {
                        case 0:
                        case 1:
                        case 2:
                            grdColumn = j + 1;
                            break;
                        case 3:
                        case 4:
                        case 5:
                            grdColumn = j + 2;  // Adjust for the spacing
                            break;
                        case 6:
                        case 7:
                        case 8:
                            grdColumn = j + 3;  // Adjust for the spacing
                            break;

                        default:
                            break;
                    }
                    Button button = new Button();
                    button.Name = "btn" + column + row;
                    button.Click += new RoutedEventHandler(btnSudoku_Click);
                    button.Margin = new Thickness(4);
                    button.Content = column + row;
                    button.Content = "";
                    

                    grdPuzzle.Children.Add(button);
                    Grid.SetColumn(button, grdColumn);
                    Grid.SetRow(button, grdRow);

                    // Create a cell object for this cell on the Sudoku grid
                    Cell cell = new Cell(grdColumn, grdRow, PuzzleSize, button);

                    // Add the cell to the list
                    MyPuzzle.Add(cell);

                }
            }
        }

        private void btnSudoku_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            MessageBox.Show(String.Format("This is a test..." + button.Name));
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
