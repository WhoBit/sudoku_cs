﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Sudoku_3
{
    class Cell
    {
        // This class holds the properties and methods for an individual cell 
        // of the Sudoku puzzle.


        public int MyColumn { get; set; }   // Which column this cell is in on the puzzle grid
        public int MyRow { get; set; }      // Which row this cell is in on the puzzle grid
        public int MySector { get; set; }   // Which sector this cell is in on the puzzle grid

        public int MyCurrentValue { get; set; }
        public Button MyButton { get; set; }

        int[] possibleValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        List<int> MyPossibleValues = new List<int>(); // { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        public Cell(int column, int row, int puzzleSize, Button button)
        {
            MyColumn = column;
            MyRow = row;
            MyCurrentValue = 0;
            MyButton = button;      // Associate this cell with the button in this cell on the grid

            // Initialize the possible values as all of the possibleValues
            for (int i = 0; i < puzzleSize; i++)
            {
                MyPossibleValues.Add(possibleValues[i]);
            }
        }

    }
}
